Source: searx
Section: web
Priority: optional
Maintainer: Johannes Schauer Marin Rodrigues <josch@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 grunt,
 node-grunt-contrib-concat,
 node-grunt-contrib-uglify,
 node-less,
 python3-all,
 python3-babel,
 python3-fontforge,
 python3-setuptools,
 python3-yaml,
Standards-Version: 4.5.0
Homepage: https://asciimoo.github.io/searx/
Vcs-Git: https://salsa.debian.org/debian/searx.git
Vcs-Browser: https://salsa.debian.org/debian/searx
Rules-Requires-Root: no

Package: python3-searx
Architecture: all
Depends:
 libjs-bootstrap,
 libjs-jquery,
 libjs-leaflet,
 libjs-requirejs,
 python3-certifi,
 python3-dateutil,
 python3-flask,
 python3-flask-babel,
 python3-lxml,
 python3-openssl,
 python3-pygments,
 python3-requests,
 python3-werkzeug (>= 0.16.1),
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 libapache2-mod-uwsgi,
 nginx | apache2 | httpd,
 uwsgi,
 uwsgi-plugin-python3,
Section: python
Description: Privacy-respecting metasearch engine - library package
 Searx is an internet metasearch engine which aggregates results from more than
 70 search services. Searx runs as a web service and provides a web interface
 that allows the user to do a general search (aggregating results from google,
 bing, yahoo) or search for files (piratebay, kickass, torrentz), images (bing,
 deviantart, google images, flickr), IT (github, stackoverflow, Arch Linux
 wiki), maps (OpenStreetMap, photon), music (youtube, spotify, soundcloud),
 news (bing news, google news, reddit), science (arxiv, wolframalpha) social
 media (digg, twitter) and videos (youtube, dailymotion, vimeo).
 .
 This package provides the Python library.

Package: searx
Architecture: all
Depends:
 python3-searx (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 nginx,
 uwsgi,
 uwsgi-plugin-python3,
Description: Privacy-respecting metasearch engine
 Searx is an internet metasearch engine which aggregates results from more than
 70 search services. Searx runs as a web service and provides a web interface
 that allows the user to do a general search (aggregating results from google,
 bing, yahoo) or search for files (piratebay, kickass, torrentz), images (bing,
 deviantart, google images, flickr), IT (github, stackoverflow, Arch Linux
 wiki), maps (OpenStreetMap, photon), music (youtube, spotify, soundcloud),
 news (bing news, google news, reddit), science (arxiv, wolframalpha) social
 media (digg, twitter) and videos (youtube, dailymotion, vimeo).
